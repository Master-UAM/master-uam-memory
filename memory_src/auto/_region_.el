(TeX-add-style-hook
 "_region_"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "11pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("hyperref" "bookmarks" "pdftex" "colorlinks=false" "pdfstartview=FitV" "linkcolor=blue" "citecolor=blue" "urlcolor=blue" "plainpages=false") ("inputenc" "latin9") ("fontenc" "T1") ("babel" "spanish") ("geometry" "top=2.5cm" "bottom=2.5cm" "left=2.5cm" "right=2.5cm") ("caption" "font=small" "labelfont=bf" "tableposition=top") ("subcaption" "font=footnotesize")))
   (TeX-run-style-hooks
    "latex2e"
    "presentacion"
    "Introduccion"
    "Componentes_y_etapas"
    "modulos_utilizados"
    "metodologia"
    "desarrollo"
    "software"
    "libreria_software"
    "aplicacion"
    "firmware"
    "comunicacion_bajo_nivel"
    "comunicacion_bajo_nivel_ii"
    "Tests"
    "comunicacion-procesador"
    "creacion_plataforma"
    "software_test"
    "fmc112"
    "fmc112_hardware"
    "control_tarjeta"
    "conclusion"
    "bibliografia"
    "article"
    "art11"
    "hyperref"
    "inputenc"
    "fontenc"
    "babel"
    "makeidx"
    "pdfcolmk"
    "eurosym"
    "url"
    "lmodern"
    "geometry"
    "layout"
    "fancyhdr"
    "graphicx"
    "caption"
    "subcaption")
   (LaTeX-add-labels
    "sec:introduccion"
    "sec:componentes_y_etapas_del_desarrollo"
    "sec:modulos_utilizados"
    "sec:metodolog�a"
    "sec:desarrollo"
    "sec:software"
    "sec:librer�a_software"
    "sec:aplicaci�n"
    "sec:firmware"
    "sec:comunicacion-bajo-nivel"
    "sec:comunicacion-bajo-nivel-ii"
    "sec:tests"
    "sec:comunicacion-procesador"
    "sec:creacion_plataforma"
    "sec:software_test"
    "sec:fmc112"
    "sec:fmc112_hardware"
    "sec:control_tarjeta"
    "sec:conclusion"
    "sec:bilbiografia")))

