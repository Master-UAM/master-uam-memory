(TeX-add-style-hook
 "tfm_presentacion"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("beamer" "table" "smaller" "10pt" "handout")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("fontenc" "T1") ("ulem" "normalem") ("babel" "spanish")))
   (TeX-run-style-hooks
    "latex2e"
    "beamer"
    "beamer10"
    "inputenc"
    "fontenc"
    "fixltx2e"
    "graphicx"
    "longtable"
    "float"
    "wrapfig"
    "ulem"
    "textcomp"
    "marvosym"
    "wasysym"
    "latexsym"
    "amssymb"
    "amstext"
    "hyperref"
    "tikz"
    "fancyvrb"
    "babel")
   (LaTeX-add-labels
    "sec-1"
    "sec-2")))

