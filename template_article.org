# Global Initialization !!!
# Options !!!

#   Document Setup
#   i) elisp links
#   - [[elisp:(pp-setup-document-article)][link: Configure as Document Article]]
# [[elisp:(pp-setup-presentation-beamer)][  - link: Configure as Presentation Beamer]]
#   ii) variable
\providebool{Document}
\booltrue{Document}
# uncomment for beamer presentation
\boolfalse{Document}

#+AUTHOR: Cayetano Santos
#+TITLE: \textbf{DAQ proposal}\linebreak\textbf{---}
#+DATE: \today
#+DESCRIPTION: Description
#+KEYWORDS: text

# #+PROPERTY: BEAMER_col_ALL 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 0.0 :ETC
# #+PROPERTY: cache no
# #+PROPERTY: exports results
# #+PROPERTY: results output
# #+PROPERTY: comments org
# #+PROPERTY: session nil
# #+PROPERTY: tangle RintroCodeOnly.R

#+BEAMER_HEADER: \usepackage{tikz}
#+BEAMER_HEADER: \usepackage{fancyvrb}
#+BEAMER_HEADER: \usepackage[english]{babel}
#+BEAMER_HEADER: \definecolor{lightgray}{gray}{0.96}
#+BEAMER_HEADER: \setlength{\tabcolsep}{1ex}
#+BEAMER_HEADER: \institute{CIEMAT, \\Centro de Investigaciones Energéticas\\Medioambientales y Tecnológicas}
#+BEAMER_HEADER: \useoutertheme{infolines}
#+BEAMER_HEADER: \setbeamersize{text margin left=2em,text margin right=2em}
#+BEAMER_HEADER: \AtBeginSection[]{\begin{frame}<beamer>\frametitle{TEXTO}\tableofcontents[currentsection]\end{frame}}
#+BEAMER: \usebackgroundtemplate{\includegraphics[width=\paperwidth]{./images/theme_cnrs_logo_white.png}}
#+BEAMER: \logo{\includegraphics[scale=0.3]{./images/theme_cnrs_logo_mini.png}}
#+BEAMER_THEME: Madrid
#+BEAMER_COLOR_THEME:
#+BEAMER_FONT_THEME:
#+BEAMER_INNER_THEME:
#+BEAMER_OUTER_THEME:

#+BEGIN_LATEX
\ifbool{Document}
{
\begin{center}Revision 1\end{center}
\newpage{}
\renewcommand{\contentsname}{Summary}
\tableofcontents
\newpage{}
\listoffigures
\newpage{}
\pagestyle{fancy}
}
{}
#+END_LATEX

#+LaTeX: \part{Context}

* Introduction                                                     :document:

  The aim  of this document is  to briefly outline  a solution to  fulfill the extended need  for an
acquisition  system  that  should  be  compliant   with  current  requirements  in  nuclear  physics
experiments.  This proposal intends  to achieve a working daq within a  reasonable time schedule and
at reduced costs when compared to commercially available solutions.

#+LaTeX: \newpage{}

* Introduction                                                 :presentation:

- Introduction

- Encore

* Requirements                                                     :document:

  Current requirements imply  the need for a  solution that must remain  modular, flexible, low-cost
and relatively easy  to set-up, operate and  maintain.  Additionally it must be  compatible with the
characteristics  of the  detectors  on  hand to  be  used, namely,  very  fast  time response  (high
bandwidth) and  high dynamic range  with several possible input  ranges available.  It  must provide
increased  on-board storage  resources (in  the order  of a  few GB  by electronic  channel), online
processing  capabilities and  readout  bandwidth of  several  hundred MB/s.   by  channel to  reduce
acquisition dead time.

  Finally,  it must  account for  a high  channel  count (up  to hundred  channels) as  well as  the
possibility to  accommodate several different  analog front ends to  account for different  types of
ancillary detectors.

\newpage{}

\part{DAQ}

\setcounter{section}{0}

* Overview                                                         :document:

  The proposed solution  is based on a  mix approach.  It relies on  existing commercially available
solutions (COTS)  coupled to home  made electronics.  The COTS  platform provides an  already proved
layer intended as  a base for the development  of custom hardware suited to the  requirements of the
daq.   The base  provides  an existing  solution  containing  memory (DDR3),  fast  readout bus  and
programmable logic (FPGA), whereas  the custom hardware in the form of a  mezzanine card is open and
may be fully defined by the user.

This   solution  relies   on  two   well   known  standards.    It   is  based   on  Pci-e   modules
[fn::http://en.wikipedia.org/wiki/Pci_express] (section  \ref{sec:pci-e}), PCI-e  infrastructure and
the    FPGA   mezzanine    standard   (FMC)    standard   [fn::http://vita.com/fmc.html]    (section
\ref{sec:fmc-fpga-mezzanine}).

** PCI-e
\label{sec:pci-e}

  It provides the concept of individual devices with very high bandwidth.  The PCI-e standard states
theoretical maximum limits of

- PCI-e Gen-1, 250 MB/s data rate per-lane

- PCI-e Gen-2, 500 MB/s data rate per-lane

- PCI-e Gen-3, 800 MB/s data rate per-lane

- PCI-e Gen-4, final specifications are expected to be released in 2014-15

  Previous  releases  are  backwards  compatible.   The  expected  payload  throughput  is  hardware
dependent, and must consider the concrete DMA implementation of the layers involved on the protocol.

** FMC (FPGA Mezzanine Card)
\label{sec:fmc-fpga-mezzanine}

  FMC is  a well-known standard  that defines I/O  mezzanine modules with  connection to an  FPGA or
other  device  with re-configurable  I/O  capability.   It allows  the  use  of existing  commercial
solutions, which may be adapted  to custom applications.  It is based on the  concept of a COTS base
board, including an FPGA,  and a mezzanine card on top of it  connected through a high-bandwidth FMC
connector.  The FMC connector provides 80 differential LVDS pairs or 160 common mode signals between
the mezzanine and the base board.  The standard  defines a series of signals (GND, Vcc, clock, etc),
mechanical and power specs, maximum limits, etc.

** Advantages of modularity and compatibility
\label{sec:advant-modul-comp}

  Modularity provides reduced development times, flexibility, customization facility, easier upgrade
of  the system  at  lower  costs and  simplified  inclusion  of new  features.   It  makes this  daq
proposition compatible with exiting COTS modules, expanding  the range of applications that might be
covered by this architecture, as explained in next section.

\newpage{}

* Architecture                                           :annotated:document:
\label{sec:architecture}

  The proposed architecture splits off in several components.  The 'node' is the core of the system,
and it is composed of a base board along with a custom, home made mezzanine card.  The expansion box
integrates the back-plane fast data bus, power  supplies and mechanical structure.  The host pc acts
as a data collector, sending data to the network or saving it to disk.

** Node (Base board + Mezzanine)
\label{sec:node-base-board}

  The node  board integrates two  digital channels running  at 1 GHZ  or one single  digital channel
running at 2 GHZ; 12 bits in both cases.

*** Base Board                                                     :ATTACH:
:PROPERTIES:
:ID:       849d6c11-48ef-498c-abd2-38811d7f0bf6
:END:
\label{sec:base-board}

   This solution relies on the existence of  a COTS platform, containing all the expected resources.
The proposed  hardware is  the HG-K7-PCIE [fn::hitechglobal.com/Boards/Kintex-7_PCIE.htm]  from High
Tech Global.  Its mains characteristics are

- Xilinx FPGA, Kintex-7 XC7K325T-2FFG900 or XC7K410T-2FFG900

- Eight-lane PCI  Express Gen 2 through  hard-coded PCI Express  controller inside the FPGA  or Gen3
  through soft IP core

- FPGA Mezzanine Connector (FMC) placed on the front panel

- DDR3 SODIMM up to 8GB (shipped with 1GB density)

- USB to UART Bridge SMA connectors for additional System and FMC clocks

#+CAPTION: Hitech Global Kintex 7 pcie
#+LABEL: fig:htg-k7-pcie
#+ATTR_LaTeX: scale=0.5, placement=h
[[file:k7-pcie.jpg]]

*** Mezzanine

  The mezzanine  board should be  custom defined and  developed.  The design  might be based  on the
great amount of similar  existing products, namely to those at cern's  open hardware repository site
[fn::http://www.ohwr.org/projects/fmc-projects],  as well  as  on equivalent  commercial modules  of
similar characteristics  [fn::http://www.4dsp.com/fmc_list.php].  The sketching of  this board would
be    based   on    similar    existing    products   -    adf-d1600    from   delphi    engineering
[fn::http://www.delphieng.com/FMC-ADC-Boards/ADF-D1600-Dual-1.6Gsps-12-bit-ADC-FMC.html].

  This  board  should  benefit  from  previous   gained  knowledge  and  experience  during  current
developments of prototyping boards, and must remain  as simple as possible. It must implement signal
conditioning and sampling of two input, DC-coupled channels.   It must include a 12 bits, 1 GS, dual
channel ADC from TI, part ADC12D1000  [fn::http://www.ti.com/product/adc12d1000]. The data bus is to
be routed to the FMC connector whether as two 12 bits buses at 1 Gbps, whether as four 12 bits buses
at  500   Mbps.   It  must  implement   the  analog  front-end   stage  based  on  an   LMH6518  VGA
[fn::http://www.ti.com/product/lmh6518] or similar from TI.  This component deals with the different
input voltages and manages  the trigger and the sampling channels through  it sampling and auxiliary
outputs.

#+CAPTION:  Delphi ADF-D1600,  Dual 1.6  Gsps,  12 bit  ADC FMC
#+LABEL: fig-adf-d1600
#+ATTR_LaTeX: scale=0.1
#+ATTR_LaTeX: width=.3, placement=h
[[file:image.jpg]]

  Additionally, the  mezzanine card should  deal with offset compensation  to benefit from  the full
coding  range,  accommodating signals  of  positive  and negative  polarity.   Further  on, it  must
implement a clock synchronization scheme to take into account the several available possibilities in
what concerns inter-board synchronization and clock management (daisy chain, star topology, etc.).

  Finally, the mezzanine board must me able to  accept a number of external signals.  These might be
whether  negative  NIM signals,  whether  positive  (LV)TTL digital  signals.   In  both cases,  the
mezzanine will simply include 50-Ohms terminated fast comparators, whose differential output will be
brought  to the  base  FPGA  through the  FMC  connector, so  that  functionality  remains open  and
programmable.  The comparator will use one of two  fixed thresholds (from a simple Vcc, two resistor
divider)  to take  into  account positive  /  negative  inputs.  Thresholds  must  be selectable  by
software.  Similarly, the mezzanine must be able to output  a number of signals from the FGPA in NIM
format.

  The front panel of the mezzanine card should  have space enought to provide two BNC connectors for
analog input signals, as well as all the remaining interface signals.

** FMC alternatives
\label{sec:fmc-alternatives}

   There    exist    a    huge    amount     of    commercially    available    FMC    COTS    cards
[fn::http://www.ohwr.org/projects/fmc-projects][fn::http://www.4dsp.com/fmc_list.php].          This
fulfills the need of being  able to process out of the box other  kinds of signals (analog, digital,
etc.).  For example, the  FCM116 from 4DSP provides 16, 14-bits, 125 MHz  channels with DC coupling,
offset compensation and 1V  and 2V scales (hardware selectable).  Any of  these cards may substitute
the custom-developed mezzanine card, as long as  the FMC standard guarantees full compatibility with
the base  board.  In  this way,  another nodes  might be used,  taking advantage  of the  concept of
modularity.

** PCI-e libraries
\label{sec:pci-e-libraries}

   In order  to implement  very fast data  transfers out  of the  node (and into  the node  at lower
speeds) the  use of a library  is mandatory.  This library  includes the firmware and  the software.
Firmware is given  in the form of a compiled  netlist (core) to be instantiated in  the FPGA design.
Software is provided  in the form of a linux  kernel module, which, after the node  is setup and the
host  pc detects  the pci-e  peripheral, will  autoload  in memory.   This working  mode provides  a
convenient, posix compatible api  from the user space software (standard read,  write, etc. calls to
an open device).

  To my knowledge, there  exist three different available libraries running on  the KC705 board from
Xilinx (the one we own). They should all be compatible with the HG-K7-PCIE from High Tech Global, as
they both include the same FPGA part, BUT THIS POINT SHOULD BE CHECKED.

*** PLDA
\label{sec:plda}

  These libraries have  been tested on the KC705  board.  They provide around 3  GB/s data transfer.
Cost is 10 kE for a single project license, 20 kE for a multiproject license.

*** Northwest Logic
\label{sec:northwest-logic}

  These libraries have been  tested and are distributed by default with the  KC705 board as provided
by Xilinx. Similar figures to the PLDA library apply here.

*** xillybus
\label{sec:xillybus}

  These libraries  have been tested  under a  recent, up to  date linux distribution.   They provide
around 600  MB/s readout from the  KC705 board.  They are  distributed open source, including  the c
source code, examples and  the kernel module.  The core netlist is protected,  but may be customized
from their web  site to improve the  performances.  They provide evaluation  and educational license
[fn::http://xillybus.com/licensing] such that

/...there is no limitation  on how the core is used, as long as the  SOLE PURPOSE of this use is
to evaluate its capabilities and fitness for a certain application .../

  For prototyping  should be  enough, for  production a full  license should  be acquired,  price is
unknown.

** Expansion Box
\label{sec:expansion-box}

  The 2-channels, PCI-e node must be mounted in  some kind of crate, providing power enought to feed
the electronics. As long as  the number of channels must scale up to tens  of them (even hundred), a
modular, compact structure would be necessary.  The  proposed solution is based on a pci-e expansion
box from magma [fn::http://www.magma.com/expressbox-16-basic].  It includes 16 Gen-2, x16 lane pci-e
slots to  accommodate the  nodes, as  well as  redundant, scalable  power supply.   Additionally, it
provides one Gen-2, x16 lane pci-e dedicated slot intended for data transfer to the host Pl.

  The rear panel  of the expansion box should  have space enought to provide two  BNC connectors for
analog input signals, as well as all the remaining interface signals. It might be wishful to preview
the use of  15 out of the 16  available slots for node  cards.  The 8th slot should  be reserved for
data  and  slow   control  management,  synchronization  and  higher  level   of  abstraction  logic
functionality. This makes that 30 digital channels may run in a single expansion box. Higher channel
counts would require pile-up of boxes.

  There exist  other alternative expansion  boxes, buts the philosophy  remains the same.   It seems
reasonable to  expect and increase in  performances in next generation  boxes, as long as  the Gen-3
pci-e  standard becomes  the  the-facto standard.   This  fact would  greatly  improve data  readout
conditions to a Gen-3 capable host pc.

** Host PC

  Following  with the  modular  approach of  this  daq  proposition, the  data  collection from  the
2-channel node  cards, thought the  expansion box to  the host pc  might be open  implemented.  This
means that no  fixed solution exist.  One possibility would  be the use of a sx79r5  shuttle pc.  It
offers          powerfull         capabilities          in         a          reduced         volume
[fn::http://global.shuttle.com/main/productsDetail?productId=1607], including  an i7  last (almost!)
generation 8 core cpu, usb3 ports, 2 GB ethernet interfaces  and up to 12 TB of sata 3 storage. Most
important, it may handle up to 32 GB of DDR3 memory and two x16 lanes, Gen-3 Pci-e slots.

  One of the available slots should be dedicated to data collection from the expansion box, as it is
already Gen-3 ready. The second pci-e slot might implement a double function, as depicted in the two
next sections of this document.

** RAID disk storage
\label{sec:raid-disk-storage}

  Data collection from the node cards, through the expansion box goes to the host pc.  It includes a
second  pci-e slot  which might  be used  to accommodate  a raid  storage expansion  disk, providing
several tens of TB of  storage at increased speed.  This data disk requires a  free slot on the host
pc to operate.

** Network data transfert
\label{sec:netw-data-transf}

  Following previous sections,  a second alternative for  data handling would be data  transfer to a
high-speed network.  The second pci-e slot on the host pc might be used to accommodate a low profile
pci-e               board               similar              to               this               one
[fn::http://www.plda.com/products/boards/low-profile/xilinx/xpressv7-lp-le].   This  model  includes
Quad-10GbE over QSFP+ and Gen-3 pci-e bandwidth.

  There  exist other  alternatives,  varying the  form  factor  and the  interface  to the  network.
Following the  application, the  network card model  to be  included in this  proposal may  vary, of
course, but  the possibilities  are broad  and are  expected to  increased performances  during next
year. They should all be compatible as long as they include a pci-e interface.

\newpage{}

* Processing capabilities                                          :document:
\label{sec:proc-capab}

   Online  data  processing  withing  the  FPGA  logic   would  be  based  on  Xilinx's  Vivado  HLS
[fn::http://www.xilinx.com/support/documentation/application_notes/xapp1163.pdf],
[fn::http://issuu.com/xcelljournal/docs/xcell_journal_issue_81,               page              28],
[fn::http://issuu.com/xcelljournal/docs/xcell_journal_issue_81,               page              38],
[fn::http://www.programmableplanet.com/author.asp?section_id=2010&doc_id=244443].     It   provides
c-coding capabilities to embed custom applications inside recent FPGA.

\newpage{}

* Summary of performances                                          :document:
\label{sec:summary-performances}

** FPGA Kindex 7FPGA Kindex 7                                       :ATTACH:
:PROPERTIES:
:Attachments: ds180_7Series_Overview.pdf
:ID:       49e58bc1-1299-47a2-b033-da52ffbf56f8
:END:
\label{sec:fpga-kindex-7fpga}

** Memory (amount, bandwidth)
\label{sec:memory-amount-bandw}

   The amount  of available memory by  channel would be  of 4 GB,  as stated by the  HG-K7-PCIE data
sheet, depending  on the availability of  the DDR3 memory  modules.  These memory modules  provide 8
bytes data buses running at 800 MHz on double data rate  mode, which comes to a 800 x 2 x 8 bytes by
second bandwidth estimate.

** Readout bandwidth
\label{sec:readout-bandwidth}

*** by node
\label{sec:node}

    Dependent on the pci-e libraries to be used. Xillybus provides around 500 MB/s by device.

*** by expansion box
\label{sec:expansion-box-1}

  The theoretical limit of bandwidth  by expansion box is 500 MB (Gen 2)  times 16 (x16 lane), which
equals 8 GB/s. This bandwidth would serve 30 channels,  which is equivalent to a maximum of 266 MB/s
by channel  or 533 MB/s  by node.  This  means that pci-e  libraries providing higher  data transfer
capabilities are not necessary.

It must  be noted  that the existence  of faster  expansion boxes, compatible  with the  Gen-3 pci-e
express standard  (x2 faster than Gen-2),  is to be  expected. This would increase  previous figures
accordingly.

*** by host pc
\label{sec:host-pc-1}

  The proposed host pc  includes two Gen-3 pci-e slots, up  to 32 GB of DDR3 RAM  memory, two 1 Gbps
interface network cards  and up to 12 TB  of sata-3 data storage.  Of  course, others possibilities,
including different form factors, exist and are acceptable.

*** by data disk
\label{sec:data-disk}

  Unknown.

*** by network card
\label{sec:network-card}

  Two and four port 10 Gb ethernet, pci-e gen-3 cards exist.

\newpage{}

\part{Development}

\setcounter{section}{0}

* Calendar                                                         :document:
\label{sec:calendar}

  This section  depicts a  tentative time  schedule for the  development of  this proposal,  from an
initial evaluation of feasibility up to the final installation on an experimental site.  It is based
on the  use of the xillybus  pci-e libraries as a  simple and straightforward way  to implement slow
control communication and data readout to / from the node.

  It is  important to  note that the  next steps  are not exclusive,  and some of  the tasks  may be
executed in parallel as they overlay as they are not mutually exclusive.

** Base board evaluation (2 weeks)
\label{sec:base-board-eval}

  First step consist on the evaluation of the coupling between the xillybus pci-e library along with
the HG-K7-PCIE board. The xillybus core has been developed to be used with Xilinx KC705 board, which
means  that it  is fully  compatible  with routing,  pinout  and main  FPGA characteristics  (Kindex
7). This configuration has already been checked.

  The xillybus  is assumed to be  compliant with another  boards with similar resources,  namely the
same or equivalent main FPGA (Kindex 7).  This  is the case of the HG-K7-PCIE.  However, the routing
and pinout are obviously different.  As a first step  and as a proof of concept, the xillybus should
be put to run using this board.

  This step implies  a verification of resources used  by the core, routing of the  logic within the
FPGA and checkout of possible modifications to the default pinout assignment.  Further on, it should
be measured the real performances of this library in terms of data transfer speed.

  This evaluation implies the use of the HG-K7-PCIE in stand alone mode on a single host pc.

** Multi-card prototype on a host pc (1 week)
\label{sec:multi-card-prototype}

  As a second step towards  the realization of this daq proposal, the feasability  of the use of the
xillybus libraries running in two pci-e devices must be checked.  This implies the use of two boards
in parallel, the HG-K7-PCIE along with the KC705.  This step includes the evaluation of performances
on a single, two pci-e slots host pc as well as the use of the provided software libraries.  At this
point, the data transfer performances of two devices must be checked.

** Multi-card prototype on an expansion box (2 weeks)
\label{sec:multi-card-prototype-1}

  As an  extension to the previous,  the same test  should be carried  on by using an  expansion box
coupled to a host pc. The two test boards should be installed in the box, which is plugged to a host
pc. The performances of this arrangement should be compared to the ones obtained in previous stage.

  At this  point, two peripheral devices  should be running at  full speed on an  expansion box. The
host pc should be collecting data from the peripherals.

** Multi-card data management (2 weeks)
\label{sec:multi-card-data}

  This stage implies the verification, whether of data  saving to disk, whether of data sending over
the network. It is strongly dependent on available resources.

** Mezzanine card development: first prototype (3 months)
\label{sec:mezz-card-devel}

  In  parallel  to  some  of  the  previous   stages,  the  mezzanine  card  development  should  be
executed. This includes an on depth study of functionalities, reference designs and part selection.

** Mezzanine card development: second prototype (3 months)
\label{sec:mezz-card-devel-1}

  Some errors are  assumed to occur during  the first prototyping stage.  This  second step includes
all  necessary tests,  verification and  corrections to  be  included in  the final  release of  the
prototype mezzanine board.

** 4 Channels, full demonstrator (1 month)
\label{sec:4-channels-full}

  At  this point,  a  first  demonstrator of  the  daq  should be  available.   It  includes a  full
operational, 4 channels, 12-bits,  1 GHZ daq system running on an expansion  box plugged to one host
pc. Data management should be able to save data on disk of send it to the network.

  This demonstrator  should be compliant with  the previous requirements.   It must be able  to deal
with signals from four detectors, including synchronization, interfacing to external systems, timing
and raw  data handling. This  means data saving  to onboard  DDR3 and data  readout to the  host for
further  management.   The demonstrator  should  be  put to  work  under  real conditions  (whenever
possible), being such that it might be decided to  expand the number of available channels up to the
full filling of the expansion box slots, which means 30 channels available.

** Online processing capabilities (1 month)
\label{sec:online-proc-capab}

  This stage  completes the  4 channels  demonstrator.  It  implies the  necessary steps  toward the
integration of  a working framework intended  for the development  of online processing on  the main
node FPGA. This means  providing to the user the tools  (interface, protocol, documentation, working
examples) to write c code to be run online.

\subsection{Study of production (1 month)} \label{sec:study-production-1}

  At  the point  where a  4 channels  demonstrator  has been  validated under  real conditions,  the
question  about how  to move  to  a higher  channel count  arises.   This question  must be  studied
carefully, as several different possibilities exist.  Mass production implies lower development time
and lowers risks, at the expenses of an evident lack of customization. The solely flexibility is the
one  provided by  the  mezzanine card,  and  this daq  proposal is  strongly  dependent on  existing
commercial solutions.  Custom development  implies higher risks, lowered by the  fact that it should
be  strongly  inspired by  the  running  demonstrator,  which should  in  any  case be  an  starting
point. This option provides a great amount flexibility as explained next.

*** Mass production
\label{sec:mass-production}

  As long  as the  demonstrator has been  validated, it  might be decided  to produce  the remaining
channels by just replicating the existing ones. This implies that the demonstrator must be developed
with this possibility in mind, being compatible with this scheme.

*** Custom development
\label{sec:custom-development}

  In  order to  reduce costs,  it might  be  decided to  develop the  base board  to substitute  the
HG-K7-PCIE. The design would be strongly based on  the existing demonstrator, which would be used as
a guide. In any case,  this solution doesn't concerns the mezzanine card :  this is the main benefit
of the modularity, as explained before.

  This solution would provide some evident benefits.  First  of all, it brings money savings as long
as redundant components  on the base board  would be eliminated. Additionally, it  reduces the costs
associated to a commercial solution.

  Most  important,  the custom  development  of  hardware allows  for  the  inclusion of  additional
features.  For example, the bigger the FPGA, the higher the computing resources available.  Xilinx's
FPGA on  the 7 family  of devices are  all compatible, varying only  in what concerns  the available
resources, in  such way that  a modification  of the demonstrator,  including a bigger  part doesn't
implies a great  risk in terms of  long development times.  In  what concerns memory, 8  GB by board
should be more than enought, and the memory interface should remain identical.

  Finally, there exist  an interesting option consisting  on the inclusion of  a dedicated computing
resource which  would add  an interesting,  unique characteristic to  the daq,  differentiating this
solution from any other similar solution. This resource is an state of the art component intended to
supply 102 GFLOPS peak performance by using 64 floating point cores on a single component, including
development         of         applications          on         standard         ansi-c         code
[fn::http://www.adapteva.com/products/silicon-devices/e64g401/].

  Withing the  custom development working scheme,  two possibilities exist. Internal  development by
local manpower resources is the first option.  The second one calls for the expertise of an external
consultancy.  This second option implies additional costs, which would be compensated when producing
a high number of channels as long as the savings by board are significant.

** Mass production and testing (3 months)
\label{sec:mass-production-}

  Following with the previous discussion, and in case the first option is to be followed, this stage
accounts for fabrication delays as well as for checking of the full system.

** Custom development
\label{sec:custom-development-1}

  Unknown.

\newpage{}

* Costs estimate                                                   :document:
\label{sec:costs-estimate}

\begin{itemize}
\item Node (2 channels) ---> 2375 + 80 + 2400 = \textbf{4855 \euro}
  \begin{itemize}
  \item Carrier Board ---> 2500 -2 \% (academic) -3 \% (above 15 units) = \textbf{2375 \euro}

    \begin{itemize}

    \item HTG-K7-PCIE-325-2 ---> 2,495 \$

    \item HTG-K7-PCIE-410-2 ---> 3,295 \$

    \end{itemize}

   \item DDR3 module ---> \textbf{80 \euro}

   \item Mezzanine ---> 1800 components + 600 fabrication = \textbf{2400 \euro}

     \begin{itemize}

     \item fabrication, \textbf{600 \euro}

       \begin{itemize}

       \item PCB \& mounting \textbf{400 \euro}

       \item mechanics \textbf{200 \euro}

       \end{itemize}

     \item components, \textbf{1800 \euro}

       \begin{itemize}

       \item adc12d1000 (1 GHZ, 12 bits) \textbf{1100 \euro}

       \item CDC (clock manager) \textbf{20 \euro}

       \item Oscilator (1 GHz) \textbf{50 \euro}

       \item VGA \textbf{30 \euro}

       \item FMC connector \textbf{200 \euro}

       \item LMK0031 (clock mux) \textbf{10 \euro}

       \item power regulators \textbf{80 \euro}

       \item connectors \textbf{110 \euro}

       \item remaining components \textbf{200 \euro}

       \end{itemize}

     \end{itemize}

  \end{itemize}

\item Expansion pci-e box (one for each 30 Channels)

  \begin{itemize}

  \item Gen 2 x16, 16 slots (850 W, 70W/slot) ---> \textbf{6500 \euro}

  \item 1700 W power supply (useful ?)) \textbf{700 \euro}

  \end{itemize}

\item Headless PC, 2 pcie Gen3 x16 slots ---> \textbf{1000 \euro} (one for each 30 Channels)

\item Network card

  \begin{itemize}

  \item 10 Gb Ethernet xN pcie card ---> \euro (one for each 30 Channels)

  \end{itemize}

\item Pci-e libraries (one for each All Channels)

  \begin{itemize}

  \item xillybus (400 MB/s) (see license)

  \item PLDA (3 GB/s) ---> \textbf{10000 \euro} license mono project

  \item Northwest (3 GB/s) --->

  \end{itemize}

\end{itemize}

\newpage{}

\appendix

\section{Apendix A}

\section{Apendix B}
